#pragma once
#include <vector>

class Cart 
{
    struct Item {
        const char *id;
        int amou;
        Item(const char *id, int amou): id(id), amou(amou){};
    };
    std::vector<Item> items;
    public:
    int count();
    int amou(const char*); 
    void add(const char*, int);
    void remove(const char*);
    void clear();
};