#include "cart.h"

int Cart::count() { return items.size(); }

void Cart::add(const char *id, int amou) {
    for(auto& item: items){
        if(item.id == id){
            item.amou = amou;
            return;
        }
    }
    items.push_back(Cart::Item(id, amou));
}

void Cart::remove(const char *id) {
    for(int i = 0; i < items.size(); ++i)
    {
        if(items[i].id == id)
        {
            items.erase(items.begin()+i);
        }
    }
}

void Cart::clear() {
    items.clear();
}

int Cart::amou(const char *id){
    for(auto& item: items)
    {
        if(item.id == id)
        {
            return item.amou;
        }
    }
    return -1;
}