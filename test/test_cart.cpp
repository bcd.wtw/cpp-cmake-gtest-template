#include "gtest/gtest.h"
#include "cart.h"

TEST(Cart, EmptyOnInit)
{
    Cart cart = Cart();
    EXPECT_EQ(cart.count(), 0);
}
TEST(Cart, IncrementCountOnAddItem)
{
    Cart cart = Cart();
    cart.add("3456", 20);
    EXPECT_EQ(cart.count(), 1);
    cart.add("1234", 20);
    EXPECT_EQ(cart.count(), 2);
}
TEST(Cart, UpdateOnAddDuplicateItem)
{
    Cart cart = Cart();
    cart.add("1111", 20);
    EXPECT_EQ(cart.amou("1111"), 20);
    cart.add("1111", 30);
    EXPECT_EQ(cart.count(), 1);
    EXPECT_EQ(cart.amou("1111"), 30);
}
TEST(Cart, RemoveItem)
{
    Cart cart = Cart();
    cart.add("1111", 20);
    cart.add("1212", 30);
    EXPECT_EQ(cart.count(), 2);
    cart.remove("1212");
    EXPECT_EQ(cart.count(), 1);
    EXPECT_EQ(cart.amou("1212"), -1);
    EXPECT_EQ(cart.amou("1111"), 20);
}
TEST(Cart, ClearItem)
{
    Cart cart;
    cart.add("1000", 20);
    cart.clear();
    EXPECT_EQ(cart.count(), 0);
}